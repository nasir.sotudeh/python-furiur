Example: Fourier Series
Suppose we want to fit a Fourier series to a dataset. 
As an example, let’s take a step function:

f(x)={01if−π<x≤0if0<x<π

In the example below, we will attempt to fit this with a Fourier Series of order n=3.

y(x)=a0+∑i=1naicos(iωx)+∑i=1nbisin(iωx)